angular.module('todo', []);
function mainController($scope, $http) {
    $scope.formData = {};
    $scope.newData = {};

    // when landing on the page, get all todos and show them
    $http.get('/api/todos')
        .success(function (data) {
            $scope.todos = data;
        })
        .error(function (data) {
            console.log('Error: ' + data);
        });

    // when submitting the add form, send the text to the node API
    $scope.createTodo = function () {
        $http.post('/api/todos', $scope.formData)
            .success(function (data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.todos = data;
                console.log(data);
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });
    };

    // delete a to-do after checking it
    $scope.deleteTodo = function (id) {
        $http.delete('/api/todos/' + id)
            .success(function (data) {
                $scope.todos = data;
                console.log(data);
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });
    };

    $scope.updateTodo = function (id) {
        $http.put('/api/todos/' + id, $scope.newData)
            .success(function(data) {
                $scope.newData = {};
                $scope.todos = data;
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });
    };
}

$('input').click(function(ev) {
    console.log('clicked on input')
});